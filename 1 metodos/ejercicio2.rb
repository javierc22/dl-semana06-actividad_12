# El siguiente programa debería mostrar sí o no, sin embargo muestrar error
# Se pide identificar el error y corregirlo.

def random
  result = [true, false].sample # 'sample' escoge un valor al azar entre las opciones ingresadas en el arreglo
  return result
end

a = random

if a == true
  puts 'sí'
elsif a == false
  puts 'no'
else
  puts 'error'
end

# Mejor opción y optimizando código
def random_2
  [true, false].sample
end

# Si 'random_2" es true, imprime 'si'. Si 'random_2' es false, imprime 'no'
puts random_2 ? 'si' : 'no' 