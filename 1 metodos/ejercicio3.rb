# Escriba un método llamado check5 que devuelva true cuando
# se le pase un número mayor a 5 y false en caso contrario.

def check5(value)
  if value > 5
    return true
  else
    return false
  end
end

puts check5(5) # Debería ser false
puts check5(6) # Debería ser true

# Solución con código optimizado
def check_5(value)
  value > 5 # Si es verdadero, retorna true. Si el falso, retorna false
end

puts check_5(5) # Debería ser false
puts check_5(6) # Debería ser true