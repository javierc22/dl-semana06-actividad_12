# Dado los arrays, obtener:
# 1. La concatenación de a y b. (hint: Los elementos que se repiten en a y b pueden aparecer dos veces en el resultado).
# 2. La unión de a y b. (hint: Los elementos que se repiten en a y b NO deben aparecer repetidos en el resultado).
# 3. La intersección de a y b. (hint: El resultado debe estar compuesto por los elementos que se repiten en a y b).
# 4. Intercalar los elementos: [[1, "lunes"], [2, "martes"], [3, "miércoles"], [9, "jueves"], [12, "viernes"], [31, "sábado"], ["domingo", "domingo"]]

a = [1, 2, 3, 9, 12, 31, 'domingo']
b = %w[lunes martes miércoles jueves viernes sábado domingo]

# 1:
def function_1(array_1, array_2)
  # Concatenación de los array a y b
  print array_1 + array_2
  puts
end

# 2:
def function_2(array_1, array_2)
  # Devuelve la unión de los array, eliminando los duplicados Ej. [ "a", "b", "c" ] | [ "c", "d", "a" ] => [ "a", "b", "c", "d" ]
  print array_1 | array_2
  puts
end

# 3:
def function_3(array_1, array_2)
  # Devuelve sólo los elementos duplicados Ej. [1,2,3] & [1,2,5,6] = [1, 2]
  print array_1 & array_2 
  puts
end

# 4:
# zip: convierte cualquier argumento en una matriz
# a = [ 4, 5, 6 ]
# b = [ 7, 8, 9 ]
# a.zip(b) => [[4, 7], [5, 8], [6, 9]]

# flatten: une los arreglos dentro de un arreglo
def function_4(array_1, array_2)
  print array_1.zip(array_2).flatten
end


function_1(a, b)
function_2(a, b)
function_3(a, b)
function_4(a, b)