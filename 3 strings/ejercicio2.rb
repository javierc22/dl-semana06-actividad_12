# Construir un arreglo de los nombres de todos sus compañeros y en base a él:
# 1. Imprimir todos los elementos que excedan más de 5 caracteres.
# 2. Crear un arreglo nuevo con todos los elementos en minúscula.
# 3. Crear un método que devuelva un arreglo con la cantidad de caracteres que tiene cada nombre.

alumnos = ['Mariana', 'Javier', 'Laura', 'Molly', 'Joy']

# 1:
def function_1(array)
  array.each { |i| puts i if i.length > 5 }
end

# 2:
def function_2(array)
  array_downcase = []
  array.each { |i| array_downcase.push(i.downcase) }
  return array_downcase
end

# 3:
def function_3(array)
  array_characters = []
  array.each { |i| array_characters.push(i.length) }
  return array_characters
end


puts 'Punto 1:'
function_1(alumnos)
puts 'Punto 2:'
puts function_2(alumnos)
puts 'Punto 3:'
print function_3(alumnos)

