# Dado el array:
# 1. Eliminar el último elemento.
# 2. Eliminar el primer elemento.
# 3. Eliminar el elemento que se encuentra en la posición media,
#    si el arreglo tiene un número par de elementos entonces remover
#    el que se encuentre en la mitad izquierda, ejemplo:
#    en el arreglo [1,2,3,4] se removería el elemento 2.
# 4. Borrar el último elemento mientras ese número sea distinto a 1.
# 5. Utilizando un arreglo vacío auxiliar y operaciones de push y pop:
#    invertir el orden de los elementos en un arreglo.

arreglo = [1,2,3,9,1,4,5,2,3,6,6]

def eliminar_ultimo_elemento(array)
  array.pop
end

def eliminar_primer_elemento(array)
  array.delete_at(0) # 'delete_at()' es un método en el cual se especifíca sólo el índice a eliminar
end

def eliminar_elemento_medio(array)
  i = a.length / 2
  i -= 1 if (i / 2).pair?
  array.delete_at(i)
end

def eliminar_ultimo_elemento_distinto_uno(array)
  array.delete_at(-1) unless array.last == 1
end

def invertir_array(array)
  arrayAux = []
  arrayAux.push(array.pop) until array.length.zero? # until = hasta que la longitud del array sea cero
  return arrayAux
end

