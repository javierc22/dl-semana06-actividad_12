 # Dado el array:
 # 1. Crear un método para eliminar todos los números pares del arreglo.
 # 2. Crear un método para obtener la suma de todos los elementos del arreglo Utilizando .each
 # 3. Crear un método para obtener el promedio de un arreglo.
 # 4. Crear un método que incrementa todos los elementos en una unidad y devuelva un arreglo nuevo.

arreglo = [1,2,3,9,1,4,5,2,3,6,6]

def eliminar_numeros_pares(array)
  array.each_with_index { |value, index| array.delete_at(index) if (value % 2).zero? }
end

def sumar_elementos(array)
  # array.sum
  resultado = 0
  array.each { |e| resultado += e}
  return resultado
end

def promedio_elementos(array)
  array.sum(0.0) / array.length
end

def incrementar_elementos(array)
  arrayIncrementado = []
  array.each { |e| arrayIncrementado.push( e + 1) }
  return arrayIncrementado
end

print arreglo
