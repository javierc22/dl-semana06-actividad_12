# Dado el array:
# 1. Mostrar en pantalla el primer elemento.
# 2. Mostrar en pantalla el último elemento.
# 3. Mostrar en pantalla todos los elementos.
# 4. Mostrar en pantalla todos los elementos junto con un índice.
# 5. Mostrar en pantalla todos los elementos que se encuentren en una posición par.

arreglo = [1,2,3,9,1,4,5,2,3,6,6]

def primer_elemento(array)
  array.first
end

def ultimo_elemento(array)
  array.last
end

def elementos(array)
  array.each { |e| print "#{e}, " }
  puts
end

def elementos_indice(array)
  array.each_with_index { |value, index| puts "#{index} => #{value}" }
end

def elementos_posicion_par(array)
  array.each_with_index { |value, index| puts "#{index} => #{value}" if (index % 2).zero? }
end

puts primer_elemento(arreglo)
puts ultimo_elemento(arreglo)
elementos(arreglo)
elementos_indice(arreglo)
elementos_posicion_par(arreglo)